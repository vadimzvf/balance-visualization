const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: {
        index: path.join(__dirname, '../src', 'index.tsx')
    },

    output: {
        path: path.join(__dirname, '../build'),
        filename: '[name].js',
        publicPath: '/'
    },

    module: {
        rules: [
            {
                test: /\.tsx?$/,
                exclude: /node_modules/,
                use: 'awesome-typescript-loader'
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    {
                        loader: 'typings-for-css-modules-loader',
                        options: {
                            modules: true,
                            importLoaders: 1,
                            localIdentName: '[name]__[local]___[hash:base64:5]',
                            namedExport: true,
                            camelCase: true
                        }
                    },
                    'postcss-loader'
                ]
            }
        ]
    },

    devtool: 'source-map',

    mode: 'development',

    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.json']
    },

    plugins: [
        new webpack.NoEmitOnErrorsPlugin(),
        new HtmlWebpackPlugin({
            template: 'src/index.html'
        }),
        new webpack.DefinePlugin({
            'process.env': { NODE_ENV: JSON.stringify('debug') }
        })
    ],

    devServer: {
        contentBase: path.join(__dirname, '../build'),
        compress: true,
        port: 8080,
        proxy: {}
    }
};
