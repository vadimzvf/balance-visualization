import ApolloClient from 'apollo-boost';

const client = new ApolloClient({
    uri: 'https://cash.sergees.com/graphql'
});

export default client;
