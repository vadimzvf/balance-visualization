import * as React from 'react';
import * as s from './ErrorTerminator.css';

function ErrorTerminator() {
    const handleReloadClick = React.useCallback((event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
        event.preventDefault();
        window.location.reload();
    }, []);

    return (
        <div className={s.terminator}>
            Something went wrong. Try to{' '}
            <a className={s.link} href="#" onClick={handleReloadClick}>
                reload the page
            </a>
        </div>
    );
}

export default ErrorTerminator;
