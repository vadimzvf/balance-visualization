import dayjs from 'dayjs';
import renderApp from '../testUtilities/renderApp';
import transactionGenerator from '../testUtilities/TransactionGenerator';
import { FETCH_TRANSACTIONS_QUERY } from '../timelineData/useTimelineData';

const monthAgo = dayjs()
    .subtract(1, 'month')
    .startOf('day')
    .format('YYYY-MM-DD');

test('Should render app', async () => {
    const account = 'Bank of America';
    const defaultTransaction = transactionGenerator
        .getTransaction()
        .withAccount(account)
        .withType('INCOME');
    const mocks = [
        {
            request: {
                query: FETCH_TRANSACTIONS_QUERY,
                variables: { fromDate: monthAgo }
            },
            result: {
                data: {
                    transactions: [
                        defaultTransaction
                            .withDateDay(0)
                            .withAmount(100)
                            .toObject(),
                        defaultTransaction
                            .withDateDay(0)
                            .withAmount(10)
                            .toObject(),
                        defaultTransaction
                            .withDateDay(1)
                            .withAmount(200)
                            .toObject(),
                        defaultTransaction
                            .withDateDay(3)
                            .withAmount(50)
                            .toObject()
                    ]
                }
            }
        }
    ];

    const ui = renderApp(mocks);

    await ui.findByText(dayjs(defaultTransaction.date).format('D MMM YYYY'));
});

test('Should able to select account', async () => {
    const bankTransaction = transactionGenerator
        .getTransaction()
        .withAccount('Bank of America')
        .withType('INCOME');
    const cashTransaction = transactionGenerator
        .getTransaction()
        .withAccount('Cash')
        .withType('INCOME');
    const mocks = [
        {
            request: {
                query: FETCH_TRANSACTIONS_QUERY,
                variables: { fromDate: monthAgo }
            },
            result: {
                data: {
                    transactions: [
                        bankTransaction
                            .withDateDay(0)
                            .withAmount(100)
                            .toObject(),
                        bankTransaction
                            .withDateDay(0)
                            .withAmount(10)
                            .toObject(),
                        cashTransaction
                            .withDateDay(50)
                            .withAmount(2000)
                            .toObject(),
                        cashTransaction
                            .withDateDay(50)
                            .withAmount(1800)
                            .toObject()
                    ]
                }
            }
        }
    ];

    const ui = renderApp(mocks);

    // should show XAxis with correct data
    await ui.findByText(dayjs(bankTransaction.date).format('D MMM YYYY'));
    expect(ui.getAccountSelect().value).toBe('Total'); // Total account should be selected by default

    ui.selectAccount(cashTransaction.account);

    expect(ui.container).toHaveTextContent(dayjs(cashTransaction.date).format('D MMM YYYY'));
    expect(ui.getAccountSelect().value).toBe(cashTransaction.account);
});

test('Should able to select date from', async () => {
    const transaction = transactionGenerator
        .getTransaction()
        .withAccount('Bank of America')
        .withType('INCOME');

    const twoMonthAgoTransaction = transactionGenerator
        .getTransaction()
        .withAccount('Bank of America')
        .withType('INCOME');

    const twoMonthAgo = dayjs()
        .subtract(2, 'month')
        .startOf('day')
        .format('YYYY-MM-DD');

    const mocks = [
        {
            request: { query: FETCH_TRANSACTIONS_QUERY, variables: { fromDate: monthAgo } },
            result: {
                data: {
                    transactions: [
                        transaction
                            .withDateDay(0)
                            .withAmount(100)
                            .toObject(),
                        transaction
                            .withDateDay(0)
                            .withAmount(10)
                            .toObject()
                    ]
                }
            }
        },
        {
            request: { query: FETCH_TRANSACTIONS_QUERY, variables: { fromDate: twoMonthAgo } },
            result: {
                data: {
                    transactions: [
                        twoMonthAgoTransaction
                            .withDate(twoMonthAgo)
                            .withAmount(300)
                            .toObject(),
                        twoMonthAgoTransaction
                            .withDate(twoMonthAgo)
                            .withAmount(1000)
                            .toObject()
                    ]
                }
            }
        }
    ];

    const ui = renderApp(mocks);

    await ui.findByText(dayjs(transaction.date).format('D MMM YYYY'));

    ui.selectDateFrom(twoMonthAgo);

    // should show YAxis for each account
    await ui.findByText('1400');
});

test('Should calculate balance for "total" account', async () => {
    const bankTransaction = transactionGenerator
        .getTransaction()
        .withAccount('Bank of America')
        .withType('INCOME');
    const cashTransaction = transactionGenerator
        .getTransaction()
        .withAccount('Cash')
        .withType('INCOME');

    const mocks = [
        {
            request: {
                query: FETCH_TRANSACTIONS_QUERY,
                variables: { fromDate: monthAgo }
            },
            result: {
                data: {
                    transactions: [
                        bankTransaction
                            .withDate(monthAgo)
                            .withAmount(2000)
                            .toObject(),
                        cashTransaction
                            .withDate(monthAgo)
                            .withAmount(2000)
                            .toObject()
                    ]
                }
            }
        }
    ];

    const ui = renderApp(mocks);

    await ui.findAllByText(dayjs(bankTransaction.date).format('D MMM YYYY'));

    // should calculate sum of balance for all accounts
    // Should show correct YAxis, with 4000 - as max value
    expect(ui.container).toHaveTextContent('4000');
});

test('Should hold selected account after view type change', async () => {
    const bankTransaction = transactionGenerator
        .getTransaction()
        .withAccount('Bank of America')
        .withType('INCOME');
    const cashTransaction = transactionGenerator
        .getTransaction()
        .withAccount('Cash')
        .withType('INCOME');
    const mocks = [
        {
            request: {
                query: FETCH_TRANSACTIONS_QUERY,
                variables: { fromDate: monthAgo }
            },
            result: {
                data: {
                    transactions: [
                        bankTransaction
                            .withDateDay(0)
                            .withAmount(100)
                            .toObject(),
                        cashTransaction
                            .withDateDay(50)
                            .withAmount(1800)
                            .toObject()
                    ]
                }
            }
        }
    ];

    const ui = renderApp(mocks);

    await ui.findByText(dayjs(bankTransaction.date).format('D MMM YYYY')); // wait for fetch data

    expect(ui.getAccountSelect().value).toBe('Total'); // Total account should be selected
    ui.selectAccount(cashTransaction.account);
    expect(ui.getAccountSelect().value).toBe(cashTransaction.account);

    ui.selectViewType('weeks');

    expect(ui.getAccountSelect().value).toBe(cashTransaction.account);
});

beforeEach(() => {
    window.history.pushState('', '', '/');
});
