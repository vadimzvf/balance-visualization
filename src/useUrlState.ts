import * as React from 'react';
import qs from 'qs';

type SetState<T> = (newValue: T) => void;

function getValueFromUrl(name: string) {
    const urlData = qs.parse(window.location.search, { ignoreQueryPrefix: true }) || {};

    return urlData[name] || '';
}

function useUrlState<T>(defaultValue: T, name: string): [T, SetState<T>] {
    const [state, setState] = React.useState<T>(getValueFromUrl(name) || defaultValue);

    const handleSetState = React.useCallback(
        (newValue: T) => {
            setState(newValue);
            const prevData = qs.parse(window.location.search, { ignoreQueryPrefix: true }) || {};
            window.history.pushState('', '', `/?${qs.stringify({ ...prevData, [name]: newValue })}`);
        },
        [name]
    );

    return [state, handleSetState];
}

export default useUrlState;
