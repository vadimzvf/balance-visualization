import * as React from 'react';
import dayjs from 'dayjs';
import debounce from 'lodash.debounce';
import * as s from './Datepicker.css';

interface Props {
    value: string;
    onChange: (date: string) => void;
    label: string;
    id: string;
}

function Datepicker(props: Props) {
    const { onChange, value } = props;
    const [localValue, setLocalValue] = React.useState(dayjs(value).format('YYYY-MM-DD'));

    React.useEffect(() => {
        setLocalValue(dayjs(value).format('YYYY-MM-DD'));
    }, [value]);

    const debouncedOnChange = React.useCallback(
        debounce(value => {
            onChange(value);
        }, 500),
        [onChange]
    );

    const handleChange = React.useCallback(
        (event: React.ChangeEvent<HTMLInputElement>) => {
            setLocalValue(event.target.value);

            if (event.target.value) {
                debouncedOnChange(dayjs(event.target.value).format('YYYY-MM-DD'));
            }
        },
        [debouncedOnChange]
    );

    return (
        <div className={s.wrap}>
            <label className={s.label} htmlFor={props.id}>
                {props.label}
            </label>
            <input id={props.id} className={s.input} type="date" onChange={handleChange} value={localValue} />
        </div>
    );
}

export default Datepicker;
