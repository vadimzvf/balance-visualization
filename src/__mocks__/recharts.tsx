import * as React from 'react';
import { BarChart, XAxis, YAxis, Bar, Tooltip } from 'recharts';

export { BarChart, XAxis, YAxis, Bar, Tooltip };

interface Props {
    children: React.ReactElement;
}

export function ResponsiveContainer(props: Props) {
    return (
        <div style={{ width: 800, height: 500 }}>
            {React.cloneElement(props.children, {
                width: 800,
                height: 500
            })}
        </div>
    );
}
