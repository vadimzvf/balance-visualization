import * as React from 'react';
import { render } from 'react-dom';
import { ApolloProvider } from '@apollo/react-hooks';
import './index.css';
import App from './App';
import client from './apolloClient';

const rootElem = document.getElementById('root');

if (rootElem) {
    render(
        <ApolloProvider client={client}>
            <App />
        </ApolloProvider>,
        rootElem
    );
} else {
    throw new Error('Cannot find root node');
}
