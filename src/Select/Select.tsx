import * as React from 'react';
import cls from 'classnames';
import * as s from './Select.css';

interface Props {
    value: string;
    options: string[];
    onChange: (value: string) => void;
    testId: string;
    id?: string;
    size: 'm' | 'l';
}

function Select(props: Props) {
    const { onChange, value } = props;

    const handleSelect = React.useCallback(
        (event: React.ChangeEvent<HTMLSelectElement>) => {
            onChange(event.target.value);
        },
        [onChange]
    );

    return (
        <select
            id={props.id}
            className={cls(s.select, props.size === 'l' && s.sizeL)}
            onChange={handleSelect}
            value={value}
            data-testid={props.testId}
        >
            {props.options.map(option => (
                <option key={option} value={option}>
                    {option}
                </option>
            ))}
        </select>
    );
}

export default Select;
