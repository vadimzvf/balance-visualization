export const wrap: string;
export const select: string;
export const tooltip: string;
export const transaction: string;
export const balance: string;
export const date: string;
export const income: string;
export const expense: string;
