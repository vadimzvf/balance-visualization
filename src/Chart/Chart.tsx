import * as React from 'react';
import { ResponsiveContainer, BarChart, XAxis, YAxis, Bar, Tooltip } from 'recharts';
import dayjs from 'dayjs';
import cls from 'classnames';
import { TimelinePoint, totalAccountName } from '../timelineData/parseData';
import Select from '../Select';
import * as s from './Chart.css';

interface TooltipProps {
    active: boolean;
    payload: Array<{ payload: TimelinePoint }>;
    label: number;
}

function BalanceTooltip(props: TooltipProps) {
    if (props.active) {
        const payload = props.payload[0].payload;

        return (
            <div className={s.tooltip}>
                <p className={s.balance}>{`Balance: ${Math.round(payload.balance)}`}</p>
                <p className={s.date}>{dayjs(payload.time).format('D MMM YYYY')}</p>
                {payload.transactions &&
                    payload.transactions.map(transaction => (
                        <p
                            key={transaction.id}
                            className={cls(
                                s.transaction,
                                transaction.type === 'INCOME' && s.income,
                                transaction.type === 'EXPENSE' && s.expense
                            )}
                        >
                            {transaction.type === 'INCOME' ? `+${transaction.amount}` : transaction.amount}
                        </p>
                    ))}
            </div>
        );
    }

    return null;
}

function formatTime(unixTime) {
    return dayjs(unixTime).format('D MMM YYYY');
}

interface Props {
    data: { [accountName: string]: TimelinePoint[] };
}

function Chart({ data }: Props) {
    const [selectedAccount, setAccount] = React.useState<null | string>(null);

    const accountsList = React.useMemo(() => Object.keys(data), [data]);

    React.useEffect(() => {
        if (!selectedAccount && accountsList.length > 0) {
            const hasTotalAccount = accountsList.includes(totalAccountName);

            if (hasTotalAccount) {
                setAccount(totalAccountName);
            } else {
                setAccount(accountsList[0]);
            }
        }
    }, [accountsList, selectedAccount]);

    const handleSelectAccount = React.useCallback(nextAccount => {
        setAccount(nextAccount);
    }, []);

    const timelinePintsToShow = data[selectedAccount] || [];

    return (
        <div className={s.wrap}>
            {timelinePintsToShow.length > 0 && selectedAccount ? (
                <div className={s.select}>
                    <Select
                        options={accountsList}
                        value={selectedAccount}
                        onChange={handleSelectAccount}
                        testId="account-select"
                        size="l"
                    />
                </div>
            ) : null}
            <ResponsiveContainer minWidth={500} width="80%" height={500}>
                <BarChart data={timelinePintsToShow}>
                    <Bar dataKey="balance" fill="#3182bd" />
                    <XAxis dataKey="time" tickFormatter={formatTime} stroke="#3182bd" />
                    <YAxis type="number" dataKey="balance" stroke="#3182bd" />
                    <Tooltip content={BalanceTooltip} cursor={false} />
                </BarChart>
            </ResponsiveContainer>
        </div>
    );
}

export default Chart;
