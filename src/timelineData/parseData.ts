import dayjs from 'dayjs';
import { Transaction, ViewType } from './useTimelineData';

export interface TimelinePoint {
    time: number;
    balance: number;
    transactions: Transaction[];
}

function splitTransferTransactions(transactions: Transaction[] = []): Transaction[] {
    const result: Transaction[] = [];

    for (const transaction of transactions) {
        if (transaction.type === 'TRANSFER') {
            result.push({
                ...transaction,
                destinationAmount: null,
                destinationAccount: null,
                type: 'INCOME',
                amount: transaction.destinationAmount,
                account: transaction.destinationAccount
            });
            result.push({
                ...transaction,
                destinationAmount: null,
                destinationAccount: null,
                type: 'EXPENSE',
                amount: -transaction.destinationAmount
            });

            continue;
        }

        result.push(transaction);
    }

    return result;
}

function mapByAccounts(transactions: Transaction[] = []): { [accountName: string]: Transaction[] } {
    const transactionsMappedByAccount = {};

    for (const transaction of transactions) {
        if (transactionsMappedByAccount[transaction.account]) {
            transactionsMappedByAccount[transaction.account].push(transaction);
        } else {
            transactionsMappedByAccount[transaction.account] = [transaction];
        }
    }

    return transactionsMappedByAccount;
}

function getBalance(transactions: Transaction[] = [], prevBalance: number) {
    let balance = prevBalance;

    for (const transaction of transactions) {
        if (transaction.type === 'INCOME') {
            balance += transaction.amount;
        }

        if (transaction.type === 'EXPENSE') {
            balance += transaction.amount; // expense should have negative value
        }

        if (transaction.type === 'BALANCE_ADJUSTMENT') {
            balance = transaction.amount;
        }

        if (transaction.type === 'TRANSFER') {
            throw new Error('Transfer transactions should be split into INCOME and EXPENSE');
        }
    }

    return balance;
}

export const totalAccountName = 'Total';

function getDayInterval(transactions: Transaction[] = []) {
    if (!transactions.length) {
        return { startDay: new Date(), endDay: new Date() };
    }

    return {
        startDay: new Date(transactions[0].date),
        endDay: new Date(transactions[transactions.length - 1].date)
    };
}

interface TimelineData {
    [accountName: string]: TimelinePoint[];
}

function parse(transactions: Transaction[] = [], viewType: ViewType = 'days'): TimelineData {
    if (!transactions.length) {
        return { Total: [] };
    }

    const sortedTransactions = transactions.sort((a, b) => new Date(a.date).getTime() - new Date(b.date).getTime());
    const transactionsMappedByAccount = mapByAccounts(splitTransferTransactions(sortedTransactions));
    const allAccounts = Object.keys(transactionsMappedByAccount);
    const { startDay, endDay } = getDayInterval(sortedTransactions);

    const timelineData = allAccounts.reduce((data, account) => ({ ...data, [account]: [] }), {
        [totalAccountName]: []
    });

    const prevAccountsBalances: { [account: string]: number } = {};

    const day =
        viewType === 'weeks'
            ? dayjs(startDay)
                  .startOf('week')
                  .toDate()
            : startDay;
    const intervalSize = viewType === 'weeks' ? 7 : 1;

    while (day <= endDay) {
        const time = day.getTime();

        let totalIntervalBalance = 0;
        const endInterval = dayjs(day.getTime())
            .add(intervalSize, 'day')
            .toDate();

        for (const account of allAccounts) {
            const transactionsInInterval = transactionsMappedByAccount[account].filter(transaction => {
                const date = new Date(transaction.date);
                return day <= date && date < endInterval;
            });

            const balanceInInterval = getBalance(transactionsInInterval, prevAccountsBalances[account] || 0);

            prevAccountsBalances[account] = balanceInInterval;
            totalIntervalBalance += balanceInInterval;
            timelineData[account].push({ balance: balanceInInterval, time, transactions: transactionsInInterval });
        }

        timelineData[totalAccountName].push({ time, balance: totalIntervalBalance, transactions: [] });

        day.setDate(day.getDate() + intervalSize);
    }

    return timelineData;
}

export default parse;
