import * as React from 'react';
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import parseData from './parseData';

export const FETCH_TRANSACTIONS_QUERY = gql`
    query Transactions($fromDate: String!) {
        transactions(fromDate: $fromDate) {
            id
            date
            type
            account
            amount
            destinationAccount
            destinationAmount
        }
    }
`;

export type ViewType = 'days' | 'weeks';

export type TransactionType = 'EXPENSE' | 'INCOME' | 'TRANSFER' | 'BALANCE_ADJUSTMENT';

export interface Transaction {
    id: number;
    date: string;
    type: TransactionType;
    account: string;
    amount: number;
    destinationAccount: string | null;
    destinationAmount: number | null;
}

interface FetchTransactionsResponse {
    transactions: Transaction[];
}

function useTimelineData({ fromDate, viewType }: { fromDate: string; viewType: ViewType }) {
    const { loading, error, data, refetch } = useQuery<FetchTransactionsResponse>(FETCH_TRANSACTIONS_QUERY, {
        variables: { fromDate }
    });

    const timelineData = React.useMemo(() => {
        return data ? parseData(data.transactions, viewType) : {};
    }, [data, viewType]);

    return { loading, error, refetch, data: timelineData };
}

export default useTimelineData;
