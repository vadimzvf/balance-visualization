import dayjs from 'dayjs';
import transactionGenerator from '../testUtilities/TransactionGenerator';
import parse from './parseData';

function getDateDay(date: number) {
    const target = new Date();
    target.setDate(date);
    target.setHours(0);
    target.setMinutes(0);
    target.setSeconds(0);
    target.setMilliseconds(0);

    return target.getTime();
}

test('Should handle empty array', () => {
    expect(parse([])).toStrictEqual({ Total: [] });
});

test('Should calculate sum balance', () => {
    const accountName = 'Bank of America';
    const bankAccountTransaction = transactionGenerator
        .getTransaction()
        .withAccount(accountName)
        .withDateDay(0)
        .withType('INCOME');

    const transactions = [
        // transactions with same date and same account
        bankAccountTransaction.withAmount(100).toObject(),
        bankAccountTransaction.withAmount(30).toObject(),
        bankAccountTransaction.withAmount(10).toObject()
    ];

    expect(parse(transactions)).toStrictEqual({
        [accountName]: [{ balance: 140, time: getDateDay(0), transactions }],
        Total: [{ balance: 140, time: getDateDay(0), transactions: [] }]
    });
});

test('Should calculate sum balance for different accounts', () => {
    const firstAccountName = 'Bank of America';
    const firstAccountTransaction = transactionGenerator
        .getTransaction()
        .withAccount(firstAccountName)
        .withDateDay(0)
        .withType('INCOME');

    const secondAccountName = 'Bank of Hawaii';
    const secondAccountTransaction = transactionGenerator
        .getTransaction()
        .withAccount(secondAccountName)
        .withDateDay(0)
        .withType('INCOME');

    const firstAccountTransactions = [
        firstAccountTransaction.withAmount(100).toObject(),
        firstAccountTransaction.withAmount(30).toObject(),
        firstAccountTransaction.withAmount(10).toObject()
    ];

    const secondAccountTransactions = [
        secondAccountTransaction.withAmount(30).toObject(),
        secondAccountTransaction.withAmount(10).toObject(),
        secondAccountTransaction.withAmount(0).toObject()
    ];
    expect(parse([...firstAccountTransactions, ...secondAccountTransactions])).toStrictEqual({
        [firstAccountName]: [{ balance: 140, time: getDateDay(0), transactions: firstAccountTransactions }],
        [secondAccountName]: [{ balance: 40, time: getDateDay(0), transactions: secondAccountTransactions }],
        Total: [{ balance: 180, time: getDateDay(0), transactions: [] }]
    });
});

test('Should split transactions by time', () => {
    const accountName = 'Bank of America';
    const bankAccountTransaction = transactionGenerator
        .getTransaction()
        .withAccount(accountName)
        .withType('INCOME');

    const firstDayTransactions = [
        bankAccountTransaction
            .withAmount(100)
            .withDateDay(0)
            .toObject(),
        bankAccountTransaction
            .withAmount(10)
            .withDateDay(0)
            .toObject()
    ];
    const secondDayTransactions = [
        bankAccountTransaction
            .withAmount(30)
            .withDateDay(1)
            .toObject(),
        bankAccountTransaction
            .withAmount(10)
            .withDateDay(1)
            .toObject()
    ];
    const thirdDayTransactions = [
        bankAccountTransaction
            .withAmount(20)
            .withDateDay(2)
            .toObject()
    ];

    expect(parse([...firstDayTransactions, ...secondDayTransactions, ...thirdDayTransactions])).toStrictEqual({
        [accountName]: [
            { balance: 110, time: getDateDay(0), transactions: firstDayTransactions },
            { balance: 150, time: getDateDay(1), transactions: secondDayTransactions },
            { balance: 170, time: getDateDay(2), transactions: thirdDayTransactions }
        ],
        Total: [
            { balance: 110, time: getDateDay(0), transactions: [] },
            { balance: 150, time: getDateDay(1), transactions: [] },
            { balance: 170, time: getDateDay(2), transactions: [] }
        ]
    });
});

test('Should handle expense transaction', () => {
    const accountName = 'Bank of America';
    const bankAccountTransaction = transactionGenerator.getTransaction().withAccount(accountName);

    const firstDayTransactions = [
        bankAccountTransaction
            .withAmount(100)
            .withDateDay(0)
            .withType('INCOME')
            .toObject(),
        bankAccountTransaction
            .withAmount(-10)
            .withDateDay(0)
            .withType('EXPENSE')
            .toObject()
    ];
    const secondDayTransactions = [
        bankAccountTransaction
            .withAmount(30)
            .withDateDay(1)
            .withType('INCOME')
            .toObject(),
        bankAccountTransaction
            .withAmount(-10)
            .withDateDay(1)
            .withType('EXPENSE')
            .toObject()
    ];
    const thirdDayTransactions = [
        bankAccountTransaction
            .withAmount(20)
            .withDateDay(2)
            .withType('INCOME')
            .toObject()
    ];

    expect(parse([...firstDayTransactions, ...secondDayTransactions, ...thirdDayTransactions])).toStrictEqual({
        [accountName]: [
            { balance: 90, time: getDateDay(0), transactions: firstDayTransactions },
            { balance: 110, time: getDateDay(1), transactions: secondDayTransactions },
            { balance: 130, time: getDateDay(2), transactions: thirdDayTransactions }
        ],
        Total: [
            { balance: 90, time: getDateDay(0), transactions: [] },
            { balance: 110, time: getDateDay(1), transactions: [] },
            { balance: 130, time: getDateDay(2), transactions: [] }
        ]
    });
});

test('Should handle transfer between accounts', () => {
    const firstAccountName = 'Bank of America';
    const firstAccountTransaction = transactionGenerator
        .getTransaction()
        .withType('INCOME')
        .withAccount(firstAccountName)
        .withDateDay(0);

    const secondAccountName = 'Bank of Hawaii';
    const secondAccountTransaction = transactionGenerator
        .getTransaction()
        .withType('INCOME')
        .withAccount(secondAccountName)
        .withDateDay(0);

    expect(
        parse([
            firstAccountTransaction.withAmount(100).toObject(), // set default balance
            secondAccountTransaction.withAmount(30).toObject(), // set default balance
            firstAccountTransaction
                .withType('TRANSFER')
                .withDestinationAccount(secondAccountName)
                .withDestinationAmount(90)
                .toObject()
        ])
    ).toStrictEqual({
        [firstAccountName]: [
            {
                balance: 10,
                time: getDateDay(0),
                transactions: [
                    firstAccountTransaction
                        .withType('INCOME')
                        .withAmount(100)
                        .withDestinationAccount(null)
                        .withDestinationAmount(null)
                        .toObject(),
                    firstAccountTransaction // transfer transaction was splited into two
                        .withType('EXPENSE')
                        .withAmount(-90)
                        .toObject()
                ]
            }
        ],
        [secondAccountName]: [
            {
                balance: 120,
                time: getDateDay(0),
                transactions: [
                    secondAccountTransaction
                        .withType('INCOME')
                        .withAmount(30)
                        .toObject(),
                    secondAccountTransaction
                        .withType('INCOME')
                        .withAmount(90)
                        .toObject()
                ]
            }
        ],
        Total: [{ balance: 130, time: getDateDay(0), transactions: [] }]
    });
});

test('Should handle balance adjustment', () => {
    const accountName = 'Bank of America';
    const accountTransaction = transactionGenerator
        .getTransaction()
        .withAccount(accountName)
        .withDateDay(0);

    const transactions = [
        accountTransaction // should be ignored
            .withType('INCOME')
            .withAmount(100)
            .toObject(),
        accountTransaction // should be ignored
            .withType('INCOME')
            .withAmount(10)
            .toObject(),
        accountTransaction // should be used as balance
            .withType('BALANCE_ADJUSTMENT')
            .withAmount(70)
            .toObject(),
        accountTransaction // should used after adjustment
            .withType('INCOME')
            .withAmount(40)
            .toObject()
    ];

    expect(parse(transactions)).toStrictEqual({
        [accountName]: [{ balance: 110, time: getDateDay(0), transactions }],
        Total: [{ balance: 110, time: getDateDay(0), transactions: [] }]
    });
});

test('Should calculate total balance', () => {
    const firstAccountName = 'Bank of America';
    const firstAccountTransaction = transactionGenerator
        .getTransaction()
        .withType('INCOME')
        .withAccount(firstAccountName)
        .withDateDay(0);

    const secondAccountName = 'Bank of Hawaii';
    const secondAccountTransaction = transactionGenerator
        .getTransaction()
        .withType('INCOME')
        .withAccount(secondAccountName)
        .withDateDay(0);

    expect(
        parse([
            firstAccountTransaction.withAmount(100).toObject(), // set default balance
            firstAccountTransaction
                .withType('EXPENSE')
                .withAmount(-20)
                .toObject(),
            secondAccountTransaction.withAmount(30).toObject(), // set default balance
            firstAccountTransaction
                .withType('TRANSFER')
                .withDestinationAccount(secondAccountName)
                .withDestinationAmount(90)
                .toObject()
        ])
    ).toStrictEqual({
        [firstAccountName]: [
            {
                balance: -10,
                time: getDateDay(0),
                transactions: [
                    firstAccountTransaction
                        .withAmount(100)
                        .withType('INCOME')
                        .withDestinationAccount(null)
                        .withDestinationAmount(null)
                        .toObject(), // set default balance
                    firstAccountTransaction
                        .withType('EXPENSE')
                        .withAmount(-20)
                        .toObject(),
                    firstAccountTransaction
                        .withType('EXPENSE')
                        .withAmount(-90)
                        .toObject()
                ]
            }
        ],
        [secondAccountName]: [
            {
                balance: 120,
                time: getDateDay(0),
                transactions: [
                    secondAccountTransaction.withAmount(30).toObject(),
                    secondAccountTransaction
                        .withType('INCOME')
                        .withAmount(90)
                        .toObject()
                ]
            }
        ],
        Total: [{ balance: 110, time: getDateDay(0), transactions: [] }]
    });
});

test('Should handle two balance adjustment for different accounts in total balance', () => {
    const firstAccountName = 'Bank of America';
    const firstAccountTransaction = transactionGenerator
        .getTransaction()
        .withType('BALANCE_ADJUSTMENT')
        .withAccount(firstAccountName)
        .withAmount(100)
        .withDateDay(0);

    const secondAccountName = 'Bank of Hawaii';
    const secondAccountTransaction = transactionGenerator
        .getTransaction()
        .withType('BALANCE_ADJUSTMENT')
        .withAccount(secondAccountName)
        .withAmount(30)
        .withDateDay(0);

    expect(parse([firstAccountTransaction.toObject(), secondAccountTransaction.toObject()])).toStrictEqual({
        [firstAccountName]: [{ balance: 100, time: getDateDay(0), transactions: [firstAccountTransaction.toObject()] }],
        [secondAccountName]: [
            { balance: 30, time: getDateDay(0), transactions: [secondAccountTransaction.toObject()] }
        ],
        Total: [{ balance: 130, time: getDateDay(0), transactions: [] }]
    });
});

test('Should keep all accounts balance for each time in total account', () => {
    const firstAccountName = 'Bank of America';
    const firstAccountTransaction = transactionGenerator
        .getTransaction()
        .withType('BALANCE_ADJUSTMENT')
        .withAccount(firstAccountName)
        .withDateDay(0)
        .withAmount(100)
        .toObject();

    const secondAccountName = 'Bank of Hawaii';
    const secondAccountTransaction = transactionGenerator
        .getTransaction()
        .withType('BALANCE_ADJUSTMENT')
        .withAccount(secondAccountName)
        .withDateDay(0);

    const firstDayHawaiiTransaction = secondAccountTransaction.withAmount(30).toObject();
    const secondDayHawaiiTransaction = secondAccountTransaction
        .withDateDay(1)
        .withAmount(90)
        .toObject();

    expect(parse([firstAccountTransaction, firstDayHawaiiTransaction, secondDayHawaiiTransaction])).toStrictEqual({
        [firstAccountName]: [
            { balance: 100, time: getDateDay(0), transactions: [firstAccountTransaction] },
            { balance: 100, time: getDateDay(1), transactions: [] }
        ],
        [secondAccountName]: [
            { balance: 30, time: getDateDay(0), transactions: [firstDayHawaiiTransaction] },
            { balance: 90, time: getDateDay(1), transactions: [secondDayHawaiiTransaction] }
        ],
        Total: [
            { balance: 130, time: getDateDay(0), transactions: [] },
            { balance: 190, time: getDateDay(1), transactions: [] }
        ]
    });
});

test('Should allow parse data by weeks', () => {
    const accountName = 'Bank of America';
    const transaction = transactionGenerator
        .getTransaction()
        .withType('INCOME')
        .withAccount(accountName);

    const date = dayjs('2020-02-26');

    const firstWeekTransactions = [
        transaction
            .withAmount(100)
            .withDate(date.format('YYYY-MM-DD'))
            .toObject(),
        transaction
            .withAmount(20)
            .withDate(date.date(27).format('YYYY-MM-DD'))
            .toObject()
    ];
    const secondWeekTransactions = [
        transaction
            .withAmount(30)
            .withDate(
                date
                    .month(2)
                    .date(3)
                    .format('YYYY-MM-DD')
            )
            .toObject()
    ];
    const thirdWeekTransactions = [
        transaction
            .withAmount(40)
            .withDate(
                date
                    .month(2)
                    .date(10)
                    .format('YYYY-MM-DD')
            )
            .toObject()
    ];

    const resultData = [
        {
            balance: 120,
            time: date
                .startOf('week')
                .toDate()
                .getTime(),
            transactions: firstWeekTransactions
        },
        {
            balance: 150,
            time: date
                .month(2)
                .date(3)
                .startOf('week')
                .toDate()
                .getTime(),
            transactions: secondWeekTransactions
        },
        {
            balance: 190,
            time: date
                .month(2)
                .date(10)
                .startOf('week')
                .toDate()
                .getTime(),
            transactions: thirdWeekTransactions
        }
    ];

    expect(
        parse([...firstWeekTransactions, ...secondWeekTransactions, ...thirdWeekTransactions], 'weeks')
    ).toStrictEqual({
        [accountName]: resultData,
        Total: resultData.map(point => ({ ...point, transactions: [] }))
    });
});

test('Should handle two balance adjustment in one week', () => {
    const accountName = 'Bank of America';
    const transaction = transactionGenerator.getTransaction().withAccount(accountName);

    const date = dayjs('2020-02-26');

    const firstWeekTransactions = [
        transaction
            .withAmount(100)
            .withType('INCOME')
            .withDate(date.format('YYYY-MM-DD'))
            .toObject(),
        transaction
            .withAmount(20)
            .withType('BALANCE_ADJUSTMENT') // will be ignored
            .withDate(date.date(27).format('YYYY-MM-DD'))
            .toObject(),
        transaction
            .withAmount(80)
            .withType('BALANCE_ADJUSTMENT') // will be used
            .withDate(date.date(28).format('YYYY-MM-DD'))
            .toObject()
    ];

    const thirdWeekTransactions = [
        transaction
            .withAmount(40)
            .withType('INCOME')
            .withDate(
                date
                    .month(2)
                    .date(10)
                    .format('YYYY-MM-DD')
            )
            .toObject()
    ];

    const resultData = [
        {
            balance: 80,
            time: date
                .startOf('week')
                .toDate()
                .getTime(),
            transactions: firstWeekTransactions
        },
        {
            balance: 80,
            time: date
                .month(2)
                .date(4)
                .startOf('week')
                .toDate()
                .getTime(),
            transactions: []
        },
        {
            balance: 120,
            time: date
                .month(2)
                .date(10)
                .startOf('week')
                .toDate()
                .getTime(),
            transactions: thirdWeekTransactions
        }
    ];

    expect(parse([...firstWeekTransactions, ...thirdWeekTransactions], 'weeks')).toStrictEqual({
        [accountName]: resultData,
        Total: resultData.map(point => ({ ...point, transactions: [] }))
    });
});
