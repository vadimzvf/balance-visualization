import * as React from 'react';
import * as s from './Spinner.css';

function Spinner() {
    return <div className={s.spinner} />;
}

export default Spinner;
