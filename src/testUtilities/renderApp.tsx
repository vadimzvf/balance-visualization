import '@testing-library/jest-dom/extend-expect';
import * as React from 'react';
import dayjs from 'dayjs';
import { MockedProvider } from '@apollo/react-testing';
import { render, fireEvent } from '@testing-library/react';
import { ViewType } from '../timelineData/useTimelineData';
import App from '../App';

function renderApp(mocks) {
    const ui = render(
        <MockedProvider mocks={mocks} addTypename={false}>
            <App />
        </MockedProvider>
    );

    function getAccountSelect(): HTMLSelectElement {
        const select = ui.getByTestId('account-select');
        if (!(select instanceof HTMLSelectElement)) {
            throw new Error('Cannot find account select element');
        }
        return select;
    }

    function selectAccount(accountName: string) {
        fireEvent.change(getAccountSelect(), { target: { value: accountName } });
    }

    function selectDateFrom(dateFrom: string) {
        fireEvent.change(ui.getByLabelText('Show date from'), {
            target: { value: dayjs(dateFrom).format('YYYY-MM-DD') }
        });
    }

    function selectViewType(viewType: ViewType) {
        fireEvent.change(ui.getByLabelText('Show data by'), {
            target: { value: viewType }
        });
    }

    return { ...ui, getAccountSelect, selectAccount, selectDateFrom, selectViewType };
}

export default renderApp;
