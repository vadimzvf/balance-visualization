import { Transaction, TransactionType } from '../timelineData/useTimelineData';

class TransactionGenerator implements Transaction {
    id = 0;
    date = '';
    type = 'EXPENSE' as TransactionType;
    account = '';
    amount = 0;
    destinationAccount: string | null = null;
    destinationAmount: number | null = null;

    public static getTransaction() {
        return new TransactionGenerator();
    }

    public withId(id: number) {
        this.id = id;
        return this;
    }

    public withDate(date: string) {
        this.date = date;
        return this;
    }

    /**
     * @param {number} dateDay set date by number, will be converted to ISO date
     */
    public withDateDay(dateDay: number) {
        const date = new Date();
        date.setDate(dateDay);
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        date.setMilliseconds(0);
        this.date = date.toISOString();
        return this;
    }

    public withType(type: TransactionType) {
        this.type = type;
        return this;
    }

    public withAccount(account: string) {
        this.account = account;
        return this;
    }

    public withAmount(amount: number) {
        this.amount = amount;
        return this;
    }

    public withDestinationAccount(account: string) {
        this.destinationAccount = account;
        return this;
    }

    public withDestinationAmount(amount: number) {
        this.destinationAmount = amount;
        return this;
    }

    public toObject(): Transaction {
        return {
            id: this.id,
            date: this.date,
            type: this.type,
            account: this.account,
            amount: this.amount,
            destinationAccount: this.destinationAccount,
            destinationAmount: this.destinationAmount
        };
    }
}

export default TransactionGenerator;
