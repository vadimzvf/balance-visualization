export const page: string;
export const center: string;
export const inner: string;
export const spinner: string;
export const chart: string;
export const settings: string;
export const control: string;
export const refresh: string;
