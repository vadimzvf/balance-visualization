import * as React from 'react';
import dayjs from 'dayjs';
import useTimelineData, { ViewType } from './timelineData/useTimelineData';
import useUrlState from './useUrlState';
import Spinner from './Spinner';
import ErrorTerminator from './ErrorTerminator';
import Chart from './Chart';
import Datepicker from './Datepicker';
import Select from './Select';
import * as s from './App.css';

const monthAgo = dayjs()
    .subtract(1, 'month')
    .startOf('day')
    .format('YYYY-MM-DD');

const viewTypes: ViewType[] = ['days', 'weeks'];

function App() {
    const [viewType, setViewType] = useUrlState<ViewType>('days', 'viewType');
    const [startDate, setStartDate] = useUrlState(monthAgo, 'fromDate');
    const { loading, error, data, refetch } = useTimelineData({ fromDate: startDate, viewType });

    const handleChangeStartDate = React.useCallback(
        nextDate => {
            setStartDate(nextDate);
        },
        [setStartDate]
    );

    const handleChangeViewType = React.useCallback(
        nextViewType => {
            setViewType(nextViewType);
        },
        [setViewType]
    );

    const handleRefresh = React.useCallback(() => {
        refetch();
    }, [refetch]);

    if (error) {
        return (
            <div className={s.page}>
                <div className={s.center}>
                    <ErrorTerminator />
                </div>
            </div>
        );
    }

    return (
        <div className={s.page}>
            <div className={s.center}>
                <div className={s.inner}>
                    {loading ? (
                        <div className={s.spinner}>
                            <Spinner />
                        </div>
                    ) : (
                        <div className={s.chart}>
                            <Chart data={data} />
                        </div>
                    )}
                    <div className={s.settings}>
                        <div className={s.control}>
                            <Datepicker
                                label="Show date from"
                                value={startDate}
                                onChange={handleChangeStartDate}
                                id="date-from"
                            />
                        </div>
                        <div className={s.control}>
                            <label htmlFor="view-type">Show data by</label>
                            <Select
                                value={viewType}
                                options={viewTypes}
                                onChange={handleChangeViewType}
                                size="m"
                                id="view-type"
                                testId="view-type"
                            />
                        </div>
                        <div className={s.control}>
                            <button className={s.refresh} onClick={handleRefresh}>
                                Refresh data
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default App;
