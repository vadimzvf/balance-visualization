# App preview available here: http://balance-visualization.surge.sh

## develop locally

Run this steps from project folder
Install all dependencies - `npm i`
Start local development server - `npm run start`
App will be started at the page `http://localhost:8080`

## build production

`npm run build`

## run tests

`npm run test`
